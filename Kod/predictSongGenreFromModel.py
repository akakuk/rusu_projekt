#Skripta koja koristi istrenirani model kako bi utvrdila žanr pjesme ili pjesmi iz foldera tmp 

import pydub as pd
import os
from PIL import Image
import numpy as np
from tensorflow.keras.models import load_model
from subprocess import Popen, PIPE, STDOUT

totalRes=[0,0,0,0,0]
currentPath = os.path.dirname(os.path.realpath(__file__)) 
data=[]
folderPaths=[]
toDelete=[]
folderPaths.append(currentPath+"\\tmp")
print(currentPath)
model = load_model("best_model.h5")
genre=[]
genre.append("Classical")
genre.append("Pop")
genre.append("Rock")
genre.append("Rap")
genre.append("Techno")
for path in folderPaths:
    print(path)
    for r, d, f in os.walk(path):
        for file in f:
            if '.wav' or '.mp3' or '.flac' in file:
                tmpFile=file.replace("  "," ")
                tmpFile=tmpFile.replace("'","")
                os.rename(os.path.join(r, file),os.path.join(r, tmpFile))
                file=tmpFile
                if '.mp3' in file:
                    song=pd.AudioSegment.from_mp3(os.path.join(r, file))
                    song=song.set_channels(1)
                    song.set_frame_rate(44100)
                    song.export(os.path.join(r, file.rsplit('.m',1)[0] + ".wav"), format="wav")
                    file=file.rsplit('.',1)[0] + ".wav"
                    toDelete.append(file)
                    print(".mp3 converted as" + path + "\\" + file)
                elif '.flac' in file:
                    song=pd.AudioSegment.from_file(os.path.join(r, file), "flac")
                    song=song.set_channels(1)
                    song.set_frame_rate(44100)
                    song.export(os.path.join(r, file.rsplit('.f',1)[0]+".wav"), format="wav")
                    file=os.path.join(r, file.rsplit('.',1)[0] + ".wav")
                    toDelete.append(file)
                    print(".flac converted as" + path + "\\" + file.rsplit('.',1)[0] + ".wav")
                print("Working on: " + os.path.join(r, file))
                command = "powershell.exe sox '{}' -n spectrogram -y 129 -X 50 -m -r -o '{}'".format(os.path.join(r, file), os.path.join(r, file.rsplit('.w',1)[0] + ".png"))
                p=Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True, cwd=currentPath)
                output, errors = p.communicate()
                
                #load the full spectrogram
                img = Image.open(os.path.join(r, file.rsplit('.',1)[0] + ".png"))
                width, height = img.size
                nSamples = int(width/128)
                width - 128
                for i in range(nSamples):
                    startPixel=i*128
                    imgTmp=img.crop((startPixel,1,startPixel+128,128+1))
                    imgTmp = imgTmp.resize((128,128), resample=Image.ANTIALIAS)
                    imgData = np.asarray(imgTmp, dtype=np.uint8).reshape(128,128,1)
                    imgData = imgData/255.
                    data.append(imgData)
                    dataNP=np.array(data).reshape([-1, 128, 128, 1])
                y=model.predict(dataNP)
                data.clear()
                res=[0,0,0,0,0]
                for e in y:
                    idx=np.array(np.where(e==max(e))).reshape(1).tolist()
                    res[idx[0]]=res[idx[0]]+1
                    totalRes[idx[0]]=totalRes[idx[0]]+1
                idx=res.index(max(res))
                print(idx)
                print(res)
                res.clear()
                print(file+" song belongs to "+ genre[idx] + " genre!")
                img.close()
                os.remove(os.path.join(r, file.rsplit('.',1)[0] + ".png"))
for item in toDelete:
    os.remove(os.path.join(path,item))
print("Total:")
print(totalRes)



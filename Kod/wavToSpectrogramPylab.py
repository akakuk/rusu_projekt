#Odbačena skripta sox se pokazao kao bolji alat

import wave
from PIL import Image
import pylab
def graph_spectrogram(wav_file):
    sound_info, frame_rate = get_wav_info(wav_file)
    pylab.figure(num=None, figsize=(50, 2))
    #pylab.subplot(111)
    pylab.specgram(sound_info, Fs=frame_rate, NFFT=int(0.02*44100), noverlap=64, scale="dB", mode="psd")
    pylab.savefig('spectrogram.png')
    img = Image.open("spectrogram.png")
    img = img.resize((img.width, 128), resample=Image.ANTIALIAS)
    #img.show()
def get_wav_info(wav_file):
    wav = wave.open(wav_file, 'r')
    frames = wav.readframes(-1)
    sound_info = pylab.fromstring(frames, 'Int16')
    frame_rate = wav.getframerate()
    wav.close()
    return sound_info, frame_rate

    
#graph_spectrogram("D:\RUSU_Data\RockWav\\001 - Led Zeppelin - Stairway To Heaven.wav")
#graph_spectrogram("D:\RUSU_Data\ClassicalWav\\301 - Mozart - Eine Kleine Nachtmusic, 1st Movement.wav")
graph_spectrogram("D:\RUSU_Data\ClassicalWav\\102 - Pachelbel - Canon in D.wav")
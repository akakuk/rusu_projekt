#Izrada spektrograma iz wav datoteka, potrebno instalirati sox

from subprocess import Popen, PIPE, STDOUT
import os

currentPath = os.path.dirname(os.path.realpath(__file__)) 

folderPaths = []
folderPaths.append("D:\RUSU_Data\ClassicalWav")
folderPaths.append("D:\RUSU_Data\RockWav")
folderPaths.append("D:\RUSU_Data\RapWav")
folderPaths.append("D:\RUSU_Data\PopWav")
folderPaths.append("D:\RUSU_Data\TechnoWav")
exportFolders = {}
exportFolders["D:\RUSU_Data\ClassicalWav"] = "D:\RUSU_Data\Classical"
exportFolders["D:\RUSU_Data\RockWav"] = "D:\RUSU_Data\Pop"
exportFolders["D:\RUSU_Data\RapWav"] = "D:\RUSU_Data\Rock"
exportFolders["D:\RUSU_Data\PopWav"] = "D:\RUSU_Data\Rap"
exportFolders["D:\RUSU_Data\TechnoWav"] = "D:\RUSU_Data\Techno"

pixelPerSecond=50
i=0
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.wav' in file:
                i=i+1
            
n=i
print(str(n) + "songs found!")
i=0
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.wav' in file:
                command = "powershell.exe sox '{}' -n spectrogram -y 129 -X {} -m -r -o '{}.png'".format(path+"\\"+file,pixelPerSecond,path+"\Spectrograms128\\"+file.rsplit(".wa",1)[0])
                p=Popen(command, shell=True, stdin=PIPE, stdout=PIPE, stderr=STDOUT, close_fds=True, cwd=currentPath)
                output, errors = p.communicate()
                i=i+1
                print(i/n*100)

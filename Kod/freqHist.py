# -*- coding: utf-8 -*-
"""
Created on Mon Sep 16 23:40:42 2019

@author: Kuka2
"""

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np


img = Image.open("tmp/testPop1.png")
x,y = img.size
imgData = np.asarray(img, dtype=np.uint8).reshape(x*y,1)
# create figure and axis
fig, ax = plt.subplots()
# plot histogram
ax.hist(imgData)
plt.xlim(0, 255) 
# set title and labels
ax.set_title('PopHist')
ax.set_xlabel('Freq')
ax.set_ylabel('nOfOccurence')
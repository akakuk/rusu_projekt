# -*- coding: utf-8 -*-
"""
Created on Mon Sep 16 21:09:32 2019

@author: Kuka2
"""

import pickle
from sklearn.metrics import confusion_matrix
import numpy as np
from tensorflow.keras.utils import to_categorical
import matplotlib.pyplot as plt
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.models import load_model

print("Loading dataset...")
datasetName = "cetvrtiSet"
datasetPath = "D:\RUSU_Data\\"
X_test = pickle.load(open("{}validation_X_{}.p".format(datasetPath,datasetName), "rb" ))
y_test = pickle.load(open("{}validation_y_{}.p".format(datasetPath,datasetName), "rb" ))
dataNP=np.array(X_test).reshape([-1, 128, 128, 1])

print("Loading model...")
model = load_model("best_model.h5")

print("Predicting...")
y_predicted=model.predict(dataNP)
y_pred = np.argmax(y_predicted, axis=1) 

cm=confusion_matrix(y_test, y_pred)
print(cm)
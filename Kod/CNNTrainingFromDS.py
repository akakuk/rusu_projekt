#Skripta koja učitava već obrađeni dataset i trenira mrežu na njemu
#s vremenom zamijenila CNNTraining skriptu


import pickle
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, MaxPooling2D, Dropout
import numpy as np
from tensorflow.keras.utils import to_categorical
import matplotlib.pyplot as plt
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint

datasetToLoad='best_model.h5'

print("Building model...")
model = Sequential()
model.add(Conv2D(64, kernel_size=2, strides=1, padding='same', activation="elu", input_shape=(128,128,1)))
model.add(MaxPooling2D(2))
model.add(Conv2D(128, kernel_size=2, strides=1, activation="elu", padding="same"))
model.add(MaxPooling2D(2))
model.add(Conv2D(256, kernel_size=2, strides=1, activation="elu", padding="same"))
model.add(MaxPooling2D(2))
model.add(Conv2D(512, kernel_size=2, strides=1, activation="elu", padding="same"))
model.add(MaxPooling2D(2))
model.add(Flatten())
model.add(Dropout(0.5))
model.add(Dense(5, activation="softmax"))
model.compile(optimizer='RMSprop', loss='categorical_crossentropy', metrics=['accuracy'])

plot_model(model, to_file='model.png', show_shapes=True)

print("Loading datasets...")
datasetName = "cetvrtiSet"
datasetPath = "D:\RUSU_Data\\"
X_train = pickle.load(open("{}train_X_{}.p".format(datasetPath,datasetName), "rb" ))
y_train = pickle.load(open("{}train_y_{}.p".format(datasetPath,datasetName), "rb" ))
X_test = pickle.load(open("{}validation_X_{}.p".format(datasetPath,datasetName), "rb" ))
y_test = pickle.load(open("{}validation_y_{}.p".format(datasetPath,datasetName), "rb" ))


print("Reshapeing...")
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
X_train=np.array(X_train).reshape([-1, 128, 128, 1])
X_test=np.array(X_test).reshape([-1, 128, 128, 1])
print("Training model...")

es = EarlyStopping(monitor='val_acc', mode='max', verbose=1, patience=5)
mc = ModelCheckpoint(datasetToLoad, monitor='val_acc', mode='max', verbose=1, save_best_only=True)
history=model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=128, epochs=20, shuffle=True, verbose=0, callbacks=[es, mc])

plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()
plt.savefig("Accuracy.png")

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()
plt.savefig("Loss.png")

print("GOTOVO...GASI KOMP, MOTAJ KABLOVE")
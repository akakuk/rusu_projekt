#Skripta za rezanje spektrograma bilokoje duljine u kriške veličine 128x128

import os
from PIL import Image

folderPaths = []
folderPaths.append("D:\RUSU_Data\ClassicalWav")
folderPaths.append("D:\RUSU_Data\RockWav")
folderPaths.append("D:\RUSU_Data\RapWav")
folderPaths.append("D:\RUSU_Data\PopWav")
folderPaths.append("D:\RUSU_Data\TechnoWav")
i=0
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.png' in file:
                i=i+1
n=i
print(str(n) + "spectrograms found!")
j=0
for path in folderPaths:
    for r, d, f in os.walk(path+"\Spectrograms128\\"):
        for file in f:
            if '.png' in file:
                #load the full spectrogram
                img = Image.open(path+"\Spectrograms128\\"+file)
                width, height = img.size
                nSamples = int(width/128)
                width - 128
                j=j+1
                print(j/n*100)
                for i in range(nSamples):
                    startPixel=i*128
                    imgTmp=img.crop((startPixel,1,startPixel+128,128+1))
                    imgTmp.save(path+"\Slices128\\"+file.rsplit(".pn",1)[0]+"_slice_"+str(i)+".png")

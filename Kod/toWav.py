#Budući da je .mp3 format zaštićen nekakvom licencom morao sam konvertati sve u .wav


import pydub as pd
import os

folderPaths = []
folderPaths.append("D:\RUSU_Data\Classical")
folderPaths.append("D:\RUSU_Data\Pop")
folderPaths.append("D:\RUSU_Data\Rock")
folderPaths.append("D:\RUSU_Data\Rap")
folderPaths.append("D:\RUSU_Data\Techno")
folderGenre = {}
folderGenre["D:\RUSU_Data\Classical"] = "Classical"
folderGenre["D:\RUSU_Data\Pop"] = "Pop"
folderGenre["D:\RUSU_Data\Rock"] = "Rock"
folderGenre["D:\RUSU_Data\Rap"] = "Rap"
folderGenre["D:\RUSU_Data\Techno"] = "Techno"
i=0

files = []
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.mp3' in file:
                i=i+1
n=i
print(str(n) + "songs found!")
i=0

for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.mp3' in file:
                song=pd.AudioSegment.from_mp3(path+"\\"+file)
                name=file.rsplit(".mp",1)[0]+".wav"
                song.export(path+"Wav"+"\\"+name,format="wav")
                print(path + "\\" + file + "->" + folderGenre[path])
                i=i+1
                print(str(i/n))

#Prva skripta korištena za učitavanje skupa podataka, obradu skupa, izgradnju modela i treniranje, zamjenjena CNNTraining
#Naknadno prenamjenjena za izradu i spremanje dataseta 

import os
import pickle
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, MaxPooling2D
from PIL import Image
import numpy as np
from sklearn.utils import shuffle
from tensorflow.keras.utils import to_categorical


X_Data=[]
y_Data=[]
X_data=[]
y_data=[]
folderPaths = []
folderPaths.append("D:\RUSU_Data\ClassicalWav")
folderPaths.append("D:\RUSU_Data\RockWav")
folderPaths.append("D:\RUSU_Data\RapWav")
folderPaths.append("D:\RUSU_Data\PopWav")
folderPaths.append("D:\RUSU_Data\TechnoWav")

genre={}
genre["D:\RUSU_Data\ClassicalWav"]=0
genre["D:\RUSU_Data\PopWav"]=1
genre["D:\RUSU_Data\RockWav"]=2
genre["D:\RUSU_Data\RapWav"]=3
genre["D:\RUSU_Data\TechnoWav"]=4


#Izrada modela CNN
print("Building model...")
model = Sequential()
model.add(Conv2D(64, kernel_size=2, strides=1, activation="elu", input_shape=(128,128,1)))
model.add(MaxPooling2D(2))
model.add(Conv2D(128, kernel_size=2, strides=1, activation="elu"))
model.add(MaxPooling2D(2))
model.add(Conv2D(256, kernel_size=2, strides=1, activation="elu"))
model.add(MaxPooling2D(2))
model.add(Conv2D(512, kernel_size=2, strides=1, activation="elu"))
model.add(MaxPooling2D(2))
model.add(Flatten())
model.add(Dense(5, activation="softmax"))
model.compile(optimizer='RMSprop', loss='categorical_crossentropy', metrics=['accuracy'])
data=[]
Data=[]
i=0

#Učitavanje seta podataka na kojima će se trenirati mreža
#Učitava se jedna po jedna kriška spektrograma pa je vrlo sporo stoga je napisana modificirana verzija skripte koja učitava gotov dataset
print("Initializing dataset...")
for path in folderPaths:
    for r, d, f in os.walk(path+"\Slices"):
        for file in f:
            if '.png' in file:
                i=i+1
n=i
print(str(n) + "spectrogram slices found!")
j=0
print("Loading dataset...")
for path in folderPaths:
    for r, d, f in os.walk(path+"\Slices"):
        for file in f:
            if '.png' in file:
                img = Image.open(path+"\Slices\\"+file)
                img = img.resize((128,128), resample=Image.ANTIALIAS)
                imgData = np.asarray(img, dtype=np.uint8).reshape(128,128,1)
                imgData = imgData/255.
                data.append((imgData,int(genre[path])))
                img.close()
                j=j+1
                print(j/n*100)
        data = shuffle(data)
        Data=Data+data[1:3001]
        data.clear()
print("Shuffling data...")
Data = shuffle(Data)
X_data, y_data = zip(*Data)
"""
#Dio koji je korišten samo za provjeru jesu li svi žanrovi zastupljeni u jednakom broju
nul=0
jed=0
dva=0
tri=0
cet=0
x=0
for x in y_data:
    print(x)
    if x==0:
        nul+=1
    elif x==1:
        jed+=1
    elif x==2:
        dva+=1
    elif x==3:
        tri+=1
    elif x==4:
        cet+=1
print(nul)
print(jed)
print(dva)
print(tri)
print(cet)
"""
#Odvajanje validacijskog i trening skupa 
X_train=X_data[0:int(len(X_data)*0.7)]
y_train=y_data[0:int(len(y_data)*0.7)]
X_test=X_data[int(len(X_data)*0.7):int(len(X_data))]
y_test=y_data[int(len(y_data)*0.7):int(len(y_data))]
#Spremanje dataseta za daljnje korištenje
datasetName = "cetvrtiSet"
datasetPath = "D:\RUSU_Data\\"
pickle.dump(X_train, open("{}train_X_{}.p".format(datasetPath,datasetName), "wb" ))
pickle.dump(y_train, open("{}train_y_{}.p".format(datasetPath,datasetName), "wb" ))
pickle.dump(X_test, open("{}validation_X_{}.p".format(datasetPath,datasetName), "wb" ))
pickle.dump(y_test, open("{}validation_y_{}.p".format(datasetPath,datasetName), "wb" ))

print("Reshapeing...")
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
X_train=np.array(X_train).reshape([-1, 128, 128, 1])
X_test=np.array(X_test).reshape([-1, 128, 128, 1])
print("Training model...")
#model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=128, epochs=20)
#model.save("D:\RUSU_Data\model.h5")
print("GOTOVO...GASI KOMP, MOTAJ KABLOVE")


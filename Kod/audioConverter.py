#Skripta koja konvertira audio zapise u .mp3 oblik, mono, 44100hz

import os
import pydub as pd


folderPaths = []
folderPaths.append("D:\Preuzimanja\RUSU_Classical")
folderPaths.append("D:\Preuzimanja\RUSU_Pop")
folderPaths.append("D:\Preuzimanja\RUSU_Rock")
folderPaths.append("D:\Preuzimanja\RUSU_Rap")
folderPaths.append("D:\Preuzimanja\RUSU_Techno")
exportFolders = {}
exportFolders["D:\Preuzimanja\RUSU_Classical"] = "D:\RUSU_Data\Classical"
exportFolders["D:\Preuzimanja\RUSU_Pop"] = "D:\RUSU_Data\Pop"
exportFolders["D:\Preuzimanja\RUSU_Rock"] = "D:\RUSU_Data\Rock"
exportFolders["D:\Preuzimanja\RUSU_Rap"] = "D:\RUSU_Data\Rap"
exportFolders["D:\Preuzimanja\RUSU_Techno"] = "D:\RUSU_Data\Techno"
i=0

files = []
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.mp3' in file:
                i=i+1
            elif '.flac' in file:
                i=i+1
            elif '.m4a' in file:
                i=i+1
            elif '.wma' in file:
                i=i+1
n=i
print(str(n) + "songs found!")
i=0
# r=root, d=directories, f = files
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.mp3' in file:
                song=pd.AudioSegment.from_mp3(os.path.join(r, file))
                song=song.set_channels(1)
                song.set_frame_rate(44100)
                song.export(exportFolders[path] + "\\" + file, format="mp3")
                i=i+1
                print(exportFolders[path] + "\\" + file)
                print(str(i/n*100) + "%")
            elif '.flac' in file:
                song=pd.AudioSegment.from_file(os.path.join(r, file), "flac")
                song=song.set_channels(1)
                song.set_frame_rate(44100)
                song.export(exportFolders[path] + "\\" + file.rsplit('.',1)[0] + ".mp3", format="mp3")
                i=i+1
                print(exportFolders[path] + "\\" + file.rsplit('.',1)[0] + ".mp3")
                print(str(i/n*100) + "%")
            elif '.m4a' in file:
                song=pd.AudioSegment.from_file(os.path.join(r, file), "m4a")
                song=song.set_channels(1)
                song.set_frame_rate(44100)
                song.export(exportFolders[path] + "\\" + file.rsplit('.',1)[0] + ".mp3", format="mp3")
                i=i+1
                print(exportFolders[path] + "\\" + file.rsplit('.',1)[0] + ".mp3")
                print(str(i/n*100) + "%")
            elif '.wma' in file:
                song=pd.AudioSegment.from_file(os.path.join(r, file), "wma")
                song=song.set_channels(1)
                song.set_frame_rate(44100)
                song.export(exportFolders[path] + "\\" + file.rsplit('.',1)[0] + ".mp3", format="mp3")
                #i=i+1
                print(exportFolders[path] + "\\" + file.rsplit('.',1)[0] + ".mp3")
                print(str(i/n*100) + "%")
                



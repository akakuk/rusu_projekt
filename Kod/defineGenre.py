#Skripta koja u metapodatke audio datoteke upisuje žanr pjesme
#Tokom projekta odbačena jer sam pronašao jednostavniji način

from mutagen.easyid3 import EasyID3
import os

folderPaths = []
folderPaths.append("D:\RUSU_Data\Classical")
folderPaths.append("D:\RUSU_Data\Pop")
folderPaths.append("D:\RUSU_Data\Rock")
folderPaths.append("D:\RUSU_Data\Rap")
folderPaths.append("D:\RUSU_Data\Techno")
folderGenre = {}
folderGenre["D:\RUSU_Data\Classical"] = "Classical"
folderGenre["D:\RUSU_Data\Pop"] = "Pop"
folderGenre["D:\RUSU_Data\Rock"] = "Rock"
folderGenre["D:\RUSU_Data\Rap"] = "Rap"
folderGenre["D:\RUSU_Data\Techno"] = "Techno"
i=0

files = []
for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.mp3' in file:
                i=i+1
n=i
print(str(n) + "songs found!")
i=0

for path in folderPaths:
    for r, d, f in os.walk(path):
        for file in f:
            if '.mp3' in file:
                song = EasyID3(path+"\\"+file)
                song["genre"]=str(folderGenre[path])
                print(path + "\\" + file + "->" + folderGenre[path])
                i=i+1
                print(str(i/n))